/* Sweep
  by BARRAGAN <http://barraganstudio.com>
  This example code is in the public domain.

  modified 8 Nov 2013
  by Scott Fitzgerald
  http://www.arduino.cc/en/Tutorial/Sweep
*/

#include <Servo.h>

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 0;    // variable to store the servo position

void setup() {
  Serial.begin(9600);
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
  myservo.write(90);
  delay(15);
  Serial.print("Please input value:");
  Serial.println("1 = 30deg, 2 = 60deg, 3 = 90deg, 4 = 120deg, 5 = 180deg");
}

void loop() {
  if (Serial.available() > 0) {
    char value = Serial.read();
    if (value == '1')
    {
      myservo.write(30);
      delay(15);
      Serial.println("30deg");
    }
    else if (value == '2')
    {
      myservo.write(60);
      delay(15);
      Serial.println("60deg");
    }
    else if (value == '3')
    {
      myservo.write(90);
      delay(15);
      Serial.println("90deg");
    }
    else if (value == '4')
    {
      myservo.write(120);
      delay(15);
      Serial.println("120deg");
    }
    else if (value == '5')
    {
      myservo.write(180);
      delay(15);
      Serial.println("180deg");
    }
    else {
      Serial.print("Please input value:");
      Serial.println("1 = 30deg, 2 = 60deg, 3 = 90deg, 4 = 120deg, 5 = 180deg");
    }
  }
  /*for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
    }
    for (pos = 180; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
    }*/
}
