#include <SoftwareSerial.h>
SoftwareSerial EEBlue(10, 11); // RX | TX

char c = ' ';
boolean NL = true;

void setup()
{
  Serial.begin(9600);
  EEBlue.begin(9600);  //Baud Rate for command Mode.
  Serial.println("EEBlue started at 9600!");
}

void loop()
{

  // Feed any data from bluetooth to Terminal.
  if (EEBlue.available())
  {
    c = EEBlue.read();
    Serial.write(c);
  }

  // Feed all data from termial to bluetooth
  if (Serial.available())
  {
    c = Serial.read();
    // do not send line end characters to the HM-10
    if (c != 10 & c != 13 )
    {
      EEBlue.write(c);
    }
    // Echo the user input to the main window.
    // If there is a new line print the ">" character.
    if (NL) {
      Serial.print("\r\n>");
      NL = false;
    }
    Serial.write(c);
    if (c == 10) {
      NL = true;

    }
  }
}
