const int TransSwitch = 7;
void setup() {
  // put your setup code here, to run once:
  pinMode(TransSwitch, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available())
  {
    char switchValue = Serial.read();
    if (switchValue == '1')
    {
      digitalWrite(TransSwitch, HIGH);
      Serial.println("Motor ON");
    }
    else if (switchValue == '0')
    {
      digitalWrite(TransSwitch, LOW);
      Serial.println("Motor OFF");
    }
  }

}
