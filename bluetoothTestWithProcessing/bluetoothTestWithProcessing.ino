#define ledPin 13

#include <SoftwareSerial.h>
 
SoftwareSerial btport(10, 11); // RX | TX from arduino -> 10 -> TX, 11 -> RX of Bluetooth

int state = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  btport.begin(38400); // Default communication rate of the Bluetooth module
}
void loop() {
  if(btport.available() > 0){ // Checks whether data is comming from the serial port
    state = btport.read(); // Reads the data from the serial port
 }
 if (state == '0') {
  digitalWrite(ledPin, LOW); // Turn LED OFF
  btport.println("LED: OFF"); // Send back, to the phone, the String "LED: ON"
  state = 0;
 }
 else if (state == '1') {
  digitalWrite(ledPin, HIGH);
  btport.println("LED: ON");;
  state = 0;
 } 
}
